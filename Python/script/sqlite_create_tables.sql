CREATE TABLE utilisateur (
 id INTEGER PRIMARY KEY,
 name TEXT NOT NULL,
 email NOT NULL UNIQUE,
 montant REAL NOT NULl
);

CREATE TABLE transactions (
 created_at INTEGER PRIMARY KEY,
 user_id TEXT NOT NULL,
 target TEXT NOT NULL,
 amount REAL NOT NULL
);